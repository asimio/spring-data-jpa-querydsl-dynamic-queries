package com.asimio.demo.rest.mapper;

import java.math.BigDecimal;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import com.asimio.demo.domain.Film;
import com.asimio.demo.fixtures.FilmFixtures;
import com.asimio.demo.rest.model.Actor;
import com.asimio.demo.rest.model.FilmResource;

public class FilmResourceMapperTest {

    @Test
    public void shouldMapFromFilmToFilmResource() {
        // Given
        Film film = FilmFixtures.createFilm();
        // When
        FilmResource actual = FilmResourceMapper.INSTANCE.map(film);
        // Then
        assertFilmResource(actual);
    }

    private void assertFilmResource(FilmResource actual) {
        Assertions.assertThat(actual.getFilmId()).isEqualTo(10);
        Assertions.assertThat(actual.getTitle()).isEqualTo("Title");
        Assertions.assertThat(actual.getDescription()).isEqualTo("Description");
        Assertions.assertThat(actual.getReleaseYear()).isEqualTo("1990");
        Assertions.assertThat(actual.getLength()).isEqualTo((short) 90);
        Assertions.assertThat(actual.getRentalRate()).isEqualTo(BigDecimal.valueOf(4.50));
        Assertions.assertThat(actual.getRentalDuration()).isEqualTo((short) 48);

        Assertions.assertThat(actual.getLang()).isEqualTo("English");

        Assertions.assertThat(actual.getActors()).contains(
                new Actor("First3", "Last3"),
                new Actor("First2", "Last2"));
    }
}