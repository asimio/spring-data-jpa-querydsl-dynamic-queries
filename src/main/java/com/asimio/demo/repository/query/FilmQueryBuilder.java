package com.asimio.demo.repository.query;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.util.Optionals;
import org.springframework.util.CollectionUtils;

import com.asimio.demo.domain.Film;
import com.asimio.demo.domain.QActor;
import com.asimio.demo.domain.QCategory;
import com.asimio.demo.domain.QFilm;
import com.asimio.demo.domain.QFilmActor;
import com.asimio.demo.domain.QFilmCategory;
import com.asimio.demo.domain.QLanguage;
import com.asimio.demo.service.FilmSearchCriteria;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FilmQueryBuilder {

    private final JPAQueryFactory queryFactory;

    public JPAQuery<Film> build(FilmSearchCriteria searchCriteria) {
        JPAQuery<Film> filmQuery = this.queryFactory
                .select(QFilm.film)
                .from(QFilm.film)
                .innerJoin(QFilm.film.filmActors, QFilmActor.filmActor).fetchJoin()
                .innerJoin(QFilmActor.filmActor.actor, QActor.actor).fetchJoin()
                .innerJoin(QFilm.film.language, QLanguage.language).fetchJoin();

        this.addCategoryJoin(filmQuery, searchCriteria.getCategories());

        Predicate rentalRateBetween = this.rentalRateBetween(searchCriteria.getMinRentalRate(), searchCriteria.getMaxRentalRate());
        Predicate releaseYearEqualTo = this.releaseYearEqualTo(searchCriteria.getReleaseYear());

        // Same as ExpressionUtils.and(rentalRateBetween, releaseYearEqualTo)
        Predicate where = ExpressionUtils.allOf(rentalRateBetween, releaseYearEqualTo);
        return filmQuery.where(where);
    }

    private Predicate rentalRateBetween(Optional<BigDecimal> minRate, Optional<BigDecimal> maxRate) {
        return Optionals.mapIfAllPresent(minRate, maxRate,
            (min, max) -> QFilm.film.rentalRate.between(min, max))
        .orElseGet(() -> null);        
    }

    private Predicate releaseYearEqualTo(Optional<Long> releaseYear) {
        return releaseYear.map(relYear -> QFilm.film.releaseYear.eq(String.valueOf(relYear))).orElseGet(() -> null);
    }

    private void addCategoryJoin(JPQLQuery<Film> filmQuery, Set<String> categories) {
        if (CollectionUtils.isEmpty(categories)) {
            return;
        }
        filmQuery
            .innerJoin(QFilm.film.filmCategories, QFilmCategory.filmCategory)
            .innerJoin(QFilmCategory.filmCategory.category, QCategory.category)
                .on(QCategory.category.name.in(categories));
    }

}