package com.asimio.demo.repository.support;

import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

@FunctionalInterface
public interface JPAQueryFactoryCallback<T> {

    JPAQuery<T> doWithJPAQueryFactory(JPAQueryFactory queryFactory);
}