package com.asimio.demo.repository.support;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.CrudMethodMetadata;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.jpa.repository.support.QuerydslJpaPredicateExecutor;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.support.PageableExecutionUtils;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.AbstractJPAQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

public class AsimioQuerydslJpaRepositoryImpl<E, ID extends Serializable> extends QuerydslJpaPredicateExecutor<E>
        implements AsimioQuerydslRepository<E> {

    private final Querydsl querydsl;
    private final JPAQueryFactory queryFactory;
    private final CrudMethodMetadata metadata;

    public AsimioQuerydslJpaRepositoryImpl(JpaEntityInformation<E, ID> entityInformation, EntityManager entityManager,
            EntityPathResolver resolver, CrudMethodMetadata metadata) {

        super(entityInformation, entityManager, resolver, metadata);

        EntityPath<E> path = resolver.createPath(entityInformation.getJavaType());
        PathBuilder<E> builder = new PathBuilder<>(path.getType(), path.getMetadata());
        this.querydsl = new Querydsl(entityManager, builder);
        this.queryFactory = new JPAQueryFactory(entityManager);
        this.metadata = metadata;
    }

    @Override
    public E findOne(JPAQueryFactoryCallback<E> callback) {
        JPAQuery<E> jpaQuery = callback.doWithJPAQueryFactory(this.queryFactory);
        return jpaQuery.fetchOne();
    }

    @Override
    public List<E> findAll(JPAQueryFactoryCallback<E> callback) {
        JPAQuery<E> jpaQuery = callback.doWithJPAQueryFactory(this.queryFactory);
        return jpaQuery.fetch();
    }

    @Override
    public Page<E> findAll(Pageable pageable, JPAQueryFactoryCallback<E> callback) {
        JPAQuery<E> jpaQuery = callback.doWithJPAQueryFactory(this.queryFactory);
        return this.findPage(jpaQuery, pageable);
    }

    private Page<E> findPage(AbstractJPAQuery<E, ?> jpaQuery, Pageable pageable) {
        JPQLQuery<E> query = this.querydsl.applyPagination(pageable, jpaQuery);
        return PageableExecutionUtils.getPage(query.fetch(), pageable, query::fetchCount);
    }

}