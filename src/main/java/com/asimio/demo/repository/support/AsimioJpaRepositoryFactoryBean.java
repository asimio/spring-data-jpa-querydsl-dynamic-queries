package com.asimio.demo.repository.support;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

public class AsimioJpaRepositoryFactoryBean<T extends JpaRepository<S, I>, S, I>
        extends JpaRepositoryFactoryBean<T, S, I> {

    public AsimioJpaRepositoryFactoryBean(Class<? extends T> repositoryInterface) {
        super(repositoryInterface);
    }

    @Override
    protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {
        return new AsimioJpaRepositoryFactory(entityManager);
    }

}