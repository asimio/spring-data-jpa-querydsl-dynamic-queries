package com.asimio.demo.repository.support;

import static org.springframework.data.querydsl.QuerydslUtils.QUERY_DSL_PRESENT;

import javax.persistence.EntityManager;

import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.jpa.repository.support.CrudMethodMetadata;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryComposition.RepositoryFragments;

public class AsimioJpaRepositoryFactory extends JpaRepositoryFactory {

    public AsimioJpaRepositoryFactory(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected RepositoryFragments getRepositoryFragments(RepositoryMetadata metadata, EntityManager entityManager,
            EntityPathResolver resolver, CrudMethodMetadata crudMethodMetadata) {

        boolean isAsimioQueryDslRepository = QUERY_DSL_PRESENT
                && AsimioQuerydslRepository.class.isAssignableFrom(metadata.getRepositoryInterface());

        if (isAsimioQueryDslRepository) {
            if (metadata.isReactiveRepository()) {
                throw new InvalidDataAccessApiUsageException(
                        "Cannot combine Querydsl and reactive repository support in a single interface");
            }

            AsimioQuerydslJpaRepositoryImpl<?, ?> queryDslRepository = new AsimioQuerydslJpaRepositoryImpl<>(
                    getEntityInformation(metadata.getDomainType()),
                    entityManager,
                    resolver,
                    crudMethodMetadata
            );
            return RepositoryFragments.just(queryDslRepository);
        }

        return super.getRepositoryFragments(metadata, entityManager, resolver, crudMethodMetadata);
    }

}