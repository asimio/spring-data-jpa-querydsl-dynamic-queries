package com.asimio.demo.repository.support;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface AsimioQuerydslRepository<E> extends QuerydslPredicateExecutor<E> {

    E findOne(JPAQueryFactoryCallback<E> callback);

    List<E> findAll(JPAQueryFactoryCallback<E> callback);

    Page<E> findAll(Pageable pageable, JPAQueryFactoryCallback<E> callback);

}