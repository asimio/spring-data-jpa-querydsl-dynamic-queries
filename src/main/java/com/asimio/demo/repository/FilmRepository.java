package com.asimio.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asimio.demo.domain.Film;
import com.asimio.demo.repository.query.FilmQueryBuilder;
import com.asimio.demo.repository.support.AsimioQuerydslRepository;
import com.asimio.demo.service.FilmSearchCriteria;
import com.querydsl.jpa.impl.JPAQuery;

@Repository
public interface FilmRepository extends JpaRepository<Film, Integer>, AsimioQuerydslRepository<Film> {

    default Page<Film> findAll(FilmSearchCriteria searchCriteria, Pageable pageable) {
        return this.findAll(pageable, queryFactory -> {
            JPAQuery<Film> result = new FilmQueryBuilder(queryFactory).build(searchCriteria);
            result.setHint("org.hibernate.fetchSize", 30);
            return result;
        });
    }

}