package com.asimio.demo.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asimio.demo.domain.Film;
import com.asimio.demo.repository.FilmRepository;
import com.asimio.demo.service.DvdRentalService;
import com.asimio.demo.service.FilmSearchCriteria;

import lombok.RequiredArgsConstructor;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class DefaultDvdRentalService implements DvdRentalService {

    private final FilmRepository filmRepository;

    @Override
    public Page<Film> retrieveFilms(FilmSearchCriteria searchCriteria, Pageable page) {
        return this.filmRepository.findAll(searchCriteria, page);
    }

}