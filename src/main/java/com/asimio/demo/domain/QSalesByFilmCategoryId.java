package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSalesByFilmCategoryId is a Querydsl query type for SalesByFilmCategoryId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QSalesByFilmCategoryId extends BeanPath<SalesByFilmCategoryId> {

    private static final long serialVersionUID = -898512900L;

    public static final QSalesByFilmCategoryId salesByFilmCategoryId = new QSalesByFilmCategoryId("salesByFilmCategoryId");

    public final StringPath category = createString("category");

    public final NumberPath<java.math.BigDecimal> totalSales = createNumber("totalSales", java.math.BigDecimal.class);

    public QSalesByFilmCategoryId(String variable) {
        super(SalesByFilmCategoryId.class, forVariable(variable));
    }

    public QSalesByFilmCategoryId(Path<? extends SalesByFilmCategoryId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSalesByFilmCategoryId(PathMetadata metadata) {
        super(SalesByFilmCategoryId.class, metadata);
    }

}

