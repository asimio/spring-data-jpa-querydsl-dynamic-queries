package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QFilmList is a Querydsl query type for FilmList
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFilmList extends EntityPathBase<FilmList> {

    private static final long serialVersionUID = 781484934L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QFilmList filmList = new QFilmList("filmList");

    public final QFilmListId id;

    public QFilmList(String variable) {
        this(FilmList.class, forVariable(variable), INITS);
    }

    public QFilmList(Path<? extends FilmList> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QFilmList(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QFilmList(PathMetadata metadata, PathInits inits) {
        this(FilmList.class, metadata, inits);
    }

    public QFilmList(Class<? extends FilmList> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QFilmListId(forProperty("id")) : null;
    }

}

