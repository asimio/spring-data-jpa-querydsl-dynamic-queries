package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSalesByFilmCategory is a Querydsl query type for SalesByFilmCategory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSalesByFilmCategory extends EntityPathBase<SalesByFilmCategory> {

    private static final long serialVersionUID = -1583056127L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSalesByFilmCategory salesByFilmCategory = new QSalesByFilmCategory("salesByFilmCategory");

    public final QSalesByFilmCategoryId id;

    public QSalesByFilmCategory(String variable) {
        this(SalesByFilmCategory.class, forVariable(variable), INITS);
    }

    public QSalesByFilmCategory(Path<? extends SalesByFilmCategory> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSalesByFilmCategory(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSalesByFilmCategory(PathMetadata metadata, PathInits inits) {
        this(SalesByFilmCategory.class, metadata, inits);
    }

    public QSalesByFilmCategory(Class<? extends SalesByFilmCategory> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QSalesByFilmCategoryId(forProperty("id")) : null;
    }

}

