package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSalesByStoreId is a Querydsl query type for SalesByStoreId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QSalesByStoreId extends BeanPath<SalesByStoreId> {

    private static final long serialVersionUID = -670095843L;

    public static final QSalesByStoreId salesByStoreId = new QSalesByStoreId("salesByStoreId");

    public final StringPath manager = createString("manager");

    public final StringPath store = createString("store");

    public final NumberPath<java.math.BigDecimal> totalSales = createNumber("totalSales", java.math.BigDecimal.class);

    public QSalesByStoreId(String variable) {
        super(SalesByStoreId.class, forVariable(variable));
    }

    public QSalesByStoreId(Path<? extends SalesByStoreId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSalesByStoreId(PathMetadata metadata) {
        super(SalesByStoreId.class, metadata);
    }

}

