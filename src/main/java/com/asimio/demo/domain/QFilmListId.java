package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QFilmListId is a Querydsl query type for FilmListId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QFilmListId extends BeanPath<FilmListId> {

    private static final long serialVersionUID = -612252863L;

    public static final QFilmListId filmListId = new QFilmListId("filmListId");

    public final StringPath actors = createString("actors");

    public final StringPath category = createString("category");

    public final StringPath description = createString("description");

    public final NumberPath<Integer> fid = createNumber("fid", Integer.class);

    public final NumberPath<Short> length = createNumber("length", Short.class);

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public final StringPath rating = createString("rating");

    public final StringPath title = createString("title");

    public QFilmListId(String variable) {
        super(FilmListId.class, forVariable(variable));
    }

    public QFilmListId(Path<? extends FilmListId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFilmListId(PathMetadata metadata) {
        super(FilmListId.class, metadata);
    }

}

