package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QStaffList is a Querydsl query type for StaffList
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QStaffList extends EntityPathBase<StaffList> {

    private static final long serialVersionUID = 1370297562L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QStaffList staffList = new QStaffList("staffList");

    public final QStaffListId id;

    public QStaffList(String variable) {
        this(StaffList.class, forVariable(variable), INITS);
    }

    public QStaffList(Path<? extends StaffList> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QStaffList(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QStaffList(PathMetadata metadata, PathInits inits) {
        this(StaffList.class, metadata, inits);
    }

    public QStaffList(Class<? extends StaffList> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QStaffListId(forProperty("id")) : null;
    }

}

