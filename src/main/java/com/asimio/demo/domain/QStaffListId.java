package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QStaffListId is a Querydsl query type for StaffListId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QStaffListId extends BeanPath<StaffListId> {

    private static final long serialVersionUID = -1699000427L;

    public static final QStaffListId staffListId = new QStaffListId("staffListId");

    public final StringPath address = createString("address");

    public final StringPath city = createString("city");

    public final StringPath country = createString("country");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath name = createString("name");

    public final StringPath phone = createString("phone");

    public final NumberPath<Short> sid = createNumber("sid", Short.class);

    public final StringPath zipCode = createString("zipCode");

    public QStaffListId(String variable) {
        super(StaffListId.class, forVariable(variable));
    }

    public QStaffListId(Path<? extends StaffListId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QStaffListId(PathMetadata metadata) {
        super(StaffListId.class, metadata);
    }

}

