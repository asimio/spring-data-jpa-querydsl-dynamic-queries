package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QFilmCategory is a Querydsl query type for FilmCategory
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFilmCategory extends EntityPathBase<FilmCategory> {

    private static final long serialVersionUID = -1259283482L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QFilmCategory filmCategory = new QFilmCategory("filmCategory");

    public final QCategory category;

    public final QFilm film;

    public final QFilmCategoryId id;

    public final DateTimePath<java.util.Date> lastUpdate = createDateTime("lastUpdate", java.util.Date.class);

    public QFilmCategory(String variable) {
        this(FilmCategory.class, forVariable(variable), INITS);
    }

    public QFilmCategory(Path<? extends FilmCategory> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QFilmCategory(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QFilmCategory(PathMetadata metadata, PathInits inits) {
        this(FilmCategory.class, metadata, inits);
    }

    public QFilmCategory(Class<? extends FilmCategory> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.category = inits.isInitialized("category") ? new QCategory(forProperty("category")) : null;
        this.film = inits.isInitialized("film") ? new QFilm(forProperty("film"), inits.get("film")) : null;
        this.id = inits.isInitialized("id") ? new QFilmCategoryId(forProperty("id")) : null;
    }

}

