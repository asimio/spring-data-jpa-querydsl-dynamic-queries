package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QNicerButSlowerFilmList is a Querydsl query type for NicerButSlowerFilmList
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QNicerButSlowerFilmList extends EntityPathBase<NicerButSlowerFilmList> {

    private static final long serialVersionUID = 1531555264L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QNicerButSlowerFilmList nicerButSlowerFilmList = new QNicerButSlowerFilmList("nicerButSlowerFilmList");

    public final QNicerButSlowerFilmListId id;

    public QNicerButSlowerFilmList(String variable) {
        this(NicerButSlowerFilmList.class, forVariable(variable), INITS);
    }

    public QNicerButSlowerFilmList(Path<? extends NicerButSlowerFilmList> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QNicerButSlowerFilmList(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QNicerButSlowerFilmList(PathMetadata metadata, PathInits inits) {
        this(NicerButSlowerFilmList.class, metadata, inits);
    }

    public QNicerButSlowerFilmList(Class<? extends NicerButSlowerFilmList> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QNicerButSlowerFilmListId(forProperty("id")) : null;
    }

}

