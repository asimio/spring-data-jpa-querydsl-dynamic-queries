package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QActorInfo is a Querydsl query type for ActorInfo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QActorInfo extends EntityPathBase<ActorInfo> {

    private static final long serialVersionUID = -66904705L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QActorInfo actorInfo = new QActorInfo("actorInfo");

    public final QActorInfoId id;

    public QActorInfo(String variable) {
        this(ActorInfo.class, forVariable(variable), INITS);
    }

    public QActorInfo(Path<? extends ActorInfo> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QActorInfo(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QActorInfo(PathMetadata metadata, PathInits inits) {
        this(ActorInfo.class, metadata, inits);
    }

    public QActorInfo(Class<? extends ActorInfo> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QActorInfoId(forProperty("id")) : null;
    }

}

