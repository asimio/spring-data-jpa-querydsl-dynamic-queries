package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QFilmCategoryId is a Querydsl query type for FilmCategoryId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QFilmCategoryId extends BeanPath<FilmCategoryId> {

    private static final long serialVersionUID = 1009353633L;

    public static final QFilmCategoryId filmCategoryId = new QFilmCategoryId("filmCategoryId");

    public final NumberPath<Short> categoryId = createNumber("categoryId", Short.class);

    public final NumberPath<Short> filmId = createNumber("filmId", Short.class);

    public QFilmCategoryId(String variable) {
        super(FilmCategoryId.class, forVariable(variable));
    }

    public QFilmCategoryId(Path<? extends FilmCategoryId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFilmCategoryId(PathMetadata metadata) {
        super(FilmCategoryId.class, metadata);
    }

}

