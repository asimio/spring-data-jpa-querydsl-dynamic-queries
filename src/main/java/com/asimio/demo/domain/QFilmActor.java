package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QFilmActor is a Querydsl query type for FilmActor
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFilmActor extends EntityPathBase<FilmActor> {

    private static final long serialVersionUID = -1554107379L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QFilmActor filmActor = new QFilmActor("filmActor");

    public final QActor actor;

    public final QFilm film;

    public final QFilmActorId id;

    public final DateTimePath<java.util.Date> lastUpdate = createDateTime("lastUpdate", java.util.Date.class);

    public QFilmActor(String variable) {
        this(FilmActor.class, forVariable(variable), INITS);
    }

    public QFilmActor(Path<? extends FilmActor> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QFilmActor(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QFilmActor(PathMetadata metadata, PathInits inits) {
        this(FilmActor.class, metadata, inits);
    }

    public QFilmActor(Class<? extends FilmActor> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.actor = inits.isInitialized("actor") ? new QActor(forProperty("actor")) : null;
        this.film = inits.isInitialized("film") ? new QFilm(forProperty("film"), inits.get("film")) : null;
        this.id = inits.isInitialized("id") ? new QFilmActorId(forProperty("id")) : null;
    }

}

