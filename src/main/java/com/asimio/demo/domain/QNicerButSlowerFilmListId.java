package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QNicerButSlowerFilmListId is a Querydsl query type for NicerButSlowerFilmListId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QNicerButSlowerFilmListId extends BeanPath<NicerButSlowerFilmListId> {

    private static final long serialVersionUID = -1349171461L;

    public static final QNicerButSlowerFilmListId nicerButSlowerFilmListId = new QNicerButSlowerFilmListId("nicerButSlowerFilmListId");

    public final StringPath actors = createString("actors");

    public final StringPath category = createString("category");

    public final StringPath description = createString("description");

    public final NumberPath<Integer> fid = createNumber("fid", Integer.class);

    public final NumberPath<Short> length = createNumber("length", Short.class);

    public final NumberPath<java.math.BigDecimal> price = createNumber("price", java.math.BigDecimal.class);

    public final StringPath rating = createString("rating");

    public final StringPath title = createString("title");

    public QNicerButSlowerFilmListId(String variable) {
        super(NicerButSlowerFilmListId.class, forVariable(variable));
    }

    public QNicerButSlowerFilmListId(Path<? extends NicerButSlowerFilmListId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QNicerButSlowerFilmListId(PathMetadata metadata) {
        super(NicerButSlowerFilmListId.class, metadata);
    }

}

