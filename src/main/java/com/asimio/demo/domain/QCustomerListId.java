package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QCustomerListId is a Querydsl query type for CustomerListId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QCustomerListId extends BeanPath<CustomerListId> {

    private static final long serialVersionUID = 1740496923L;

    public static final QCustomerListId customerListId = new QCustomerListId("customerListId");

    public final StringPath address = createString("address");

    public final StringPath city = createString("city");

    public final StringPath country = createString("country");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath name = createString("name");

    public final StringPath notes = createString("notes");

    public final StringPath phone = createString("phone");

    public final NumberPath<Short> sid = createNumber("sid", Short.class);

    public final StringPath zipCode = createString("zipCode");

    public QCustomerListId(String variable) {
        super(CustomerListId.class, forVariable(variable));
    }

    public QCustomerListId(Path<? extends CustomerListId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QCustomerListId(PathMetadata metadata) {
        super(CustomerListId.class, metadata);
    }

}

