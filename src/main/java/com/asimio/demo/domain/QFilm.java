package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QFilm is a Querydsl query type for Film
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFilm extends EntityPathBase<Film> {

    private static final long serialVersionUID = 2126447304L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QFilm film = new QFilm("film");

    public final StringPath description = createString("description");

    public final SetPath<FilmActor, QFilmActor> filmActors = this.<FilmActor, QFilmActor>createSet("filmActors", FilmActor.class, QFilmActor.class, PathInits.DIRECT2);

    public final SetPath<FilmCategory, QFilmCategory> filmCategories = this.<FilmCategory, QFilmCategory>createSet("filmCategories", FilmCategory.class, QFilmCategory.class, PathInits.DIRECT2);

    public final NumberPath<Integer> filmId = createNumber("filmId", Integer.class);

    public final StringPath fulltext = createString("fulltext");

    public final SetPath<Inventory, QInventory> inventories = this.<Inventory, QInventory>createSet("inventories", Inventory.class, QInventory.class, PathInits.DIRECT2);

    public final QLanguage language;

    public final DateTimePath<java.util.Date> lastUpdate = createDateTime("lastUpdate", java.util.Date.class);

    public final NumberPath<Short> length = createNumber("length", Short.class);

    public final StringPath rating = createString("rating");

    public final StringPath releaseYear = createString("releaseYear");

    public final NumberPath<Short> rentalDuration = createNumber("rentalDuration", Short.class);

    public final NumberPath<java.math.BigDecimal> rentalRate = createNumber("rentalRate", java.math.BigDecimal.class);

    public final NumberPath<java.math.BigDecimal> replacementCost = createNumber("replacementCost", java.math.BigDecimal.class);

    public final StringPath specialFeatures = createString("specialFeatures");

    public final StringPath title = createString("title");

    public QFilm(String variable) {
        this(Film.class, forVariable(variable), INITS);
    }

    public QFilm(Path<? extends Film> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QFilm(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QFilm(PathMetadata metadata, PathInits inits) {
        this(Film.class, metadata, inits);
    }

    public QFilm(Class<? extends Film> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.language = inits.isInitialized("language") ? new QLanguage(forProperty("language")) : null;
    }

}

