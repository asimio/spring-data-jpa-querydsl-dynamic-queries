package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QSalesByStore is a Querydsl query type for SalesByStore
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSalesByStore extends EntityPathBase<SalesByStore> {

    private static final long serialVersionUID = -376115870L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QSalesByStore salesByStore = new QSalesByStore("salesByStore");

    public final QSalesByStoreId id;

    public QSalesByStore(String variable) {
        this(SalesByStore.class, forVariable(variable), INITS);
    }

    public QSalesByStore(Path<? extends SalesByStore> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QSalesByStore(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QSalesByStore(PathMetadata metadata, PathInits inits) {
        this(SalesByStore.class, metadata, inits);
    }

    public QSalesByStore(Class<? extends SalesByStore> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QSalesByStoreId(forProperty("id")) : null;
    }

}

