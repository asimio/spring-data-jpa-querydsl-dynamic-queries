package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCustomerList is a Querydsl query type for CustomerList
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCustomerList extends EntityPathBase<CustomerList> {

    private static final long serialVersionUID = -1861873952L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCustomerList customerList = new QCustomerList("customerList");

    public final QCustomerListId id;

    public QCustomerList(String variable) {
        this(CustomerList.class, forVariable(variable), INITS);
    }

    public QCustomerList(Path<? extends CustomerList> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCustomerList(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCustomerList(PathMetadata metadata, PathInits inits) {
        this(CustomerList.class, metadata, inits);
    }

    public QCustomerList(Class<? extends CustomerList> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.id = inits.isInitialized("id") ? new QCustomerListId(forProperty("id")) : null;
    }

}

