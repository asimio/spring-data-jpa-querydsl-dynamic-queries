package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QFilmActorId is a Querydsl query type for FilmActorId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QFilmActorId extends BeanPath<FilmActorId> {

    private static final long serialVersionUID = 1151430152L;

    public static final QFilmActorId filmActorId = new QFilmActorId("filmActorId");

    public final NumberPath<Short> actorId = createNumber("actorId", Short.class);

    public final NumberPath<Short> filmId = createNumber("filmId", Short.class);

    public QFilmActorId(String variable) {
        super(FilmActorId.class, forVariable(variable));
    }

    public QFilmActorId(Path<? extends FilmActorId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFilmActorId(PathMetadata metadata) {
        super(FilmActorId.class, metadata);
    }

}

