package com.asimio.demo.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QActorInfoId is a Querydsl query type for ActorInfoId
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QActorInfoId extends BeanPath<ActorInfoId> {

    private static final long serialVersionUID = 129090298L;

    public static final QActorInfoId actorInfoId = new QActorInfoId("actorInfoId");

    public final NumberPath<Integer> actorId = createNumber("actorId", Integer.class);

    public final StringPath filmInfo = createString("filmInfo");

    public final StringPath firstName = createString("firstName");

    public final StringPath lastName = createString("lastName");

    public QActorInfoId(String variable) {
        super(ActorInfoId.class, forVariable(variable));
    }

    public QActorInfoId(Path<? extends ActorInfoId> path) {
        super(path.getType(), path.getMetadata());
    }

    public QActorInfoId(PathMetadata metadata) {
        super(ActorInfoId.class, metadata);
    }

}

