# README #

Accompanying source code for blog entry at https://tech.asimio.net/2023/01/31/Writing-dynamic-SQL-queries-using-Spring-Data-JPA-Repositories-Hibernate-and-Querydsl.html

### Requirements ###

* Java 8+
* Maven 3.2.x+

### Building the artifact ###

```
mvn clean package
```

### Running the application from command line ###

```
mvn spring-boot:run
```

### Available URLs

```
curl "http://localhost:8080/api/films?minRentalRate=0.5&maxRentalRate=4.99&page=10&size=5"
curl "http://localhost:8080/api/films?releaseYear=2006&page=10&size=5"
curl "http://localhost:8080/api/films?category=Action&category=Comedy&category=Horror&minRentalRate=0.99&maxRentalRate=4.99&releaseYear=2005&page=10&size=5"
```
should result in successful responses. Please look at the logs to verify Hibernate executes different queries depending on the request parameters.

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero
